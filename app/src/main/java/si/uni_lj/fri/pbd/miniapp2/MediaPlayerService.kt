package si.uni_lj.fri.pbd.miniapp2

import android.app.*
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.media.MediaPlayer
import android.os.*
import android.util.Log
import androidx.core.app.NotificationCompat
import kotlin.system.exitProcess


class MediaPlayerService : Service() {
    companion object {
        val TAG = MediaPlayerService::class.simpleName
        //variables to compare intents with
        const val ACTION_START = "start_service"
        const val ACTION_TOGGLE = "play_pause_service"
        const val ACTION_STOP = "stop_play_service"
        const val ACTION_EXIT = "exit_service"
        //times for notification handler
        private const val MSG_UPDATE_TIME = 1
        private const val UPDATE_RATE_MS = 500L
        //ID required for notification
        const val NOTIFICATION_ID = 1
        private const val channelID = "media_player_notification"
    }

    //true if last command came from notification, else false
    var lastCommand = false
    //booleans to help
    var isSongFinished = false
        private set
    var currentTitle: String  = ""
        private set
    var currentTrackDurationIntSec: Int = 0
    var currentTrackDurationString: String = ""
        private set
    var isSongLoaded = false
        private set
    var isMusicPlaying = false
        private set //only modifiable privately

    private var player: MediaPlayer = MediaPlayer()

    private val serviceBinder = RunServiceBinder()

    var serviceBound = false
    var accelerationService: AccelerationService? = null


    private val servConnect: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binder = p1 as AccelerationService.RunServiceBinder
            accelerationService = binder.service
            serviceBound = true
            Log.d(TAG, "Service bound")
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            Log.d(TAG, "Service Unbound")
            serviceBound = false
        }
    }
    //unbind gestures
    fun gesturesOff() {
        if(serviceBound) {
            Log.d(TAG, "Disconnecting service")
            val i = Intent(this, AccelerationService::class.java)
            i.action = ACTION_STOP
            stopService(i)
            updateAcceleration.removeMessages(MSG_UPDATE_TIME)
        }
    }
    //bind gestures
    fun gesturesOn() {
        if(!serviceBound) {
            Log.d(TAG, "Connecting service")
            val i = Intent(this, AccelerationService::class.java)
            i.action = ACTION_START
            startService(i)
            bindService(i, servConnect, 0)
            updateAcceleration.sendEmptyMessage(MSG_UPDATE_TIME)
        }
    }

    //handler for checking gesture based commands
    private val updateAcceleration = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            if (MSG_UPDATE_TIME == msg.what) {
                accelerationChecker()
                sendEmptyMessageDelayed(MSG_UPDATE_TIME,UPDATE_RATE_MS)
            }
        }
    }

    //check for changes in acceleration service, act accordingly
    private fun accelerationChecker() {
        if (accelerationService?.changePlay == true) {
            Log.d(TAG, "Playing")
            accelerationService?.changePlay = false
            startTrack()
        }
        if (accelerationService?.changePause == true) {
            Log.d(TAG, "Pausing")
            accelerationService?.changePause = false
            pauseTrack()
        }
    }

    inner class RunServiceBinder: Binder() {
        val service:  MediaPlayerService
            get() = this@MediaPlayerService
    }

    override fun onBind(intent: Intent): IBinder {
        return serviceBinder
    }

    override fun onCreate() {
        super.onCreate()

        //create onCompletionListener to stop track if track completes.
        player.setOnCompletionListener {
            stopTrack()
            isSongFinished = true
        }
        createNotificationChannel()
        updateNotification()

    }

    //play button function
    fun startTrack() {
        Log.d(TAG,"Starting track")
        //ignore press in case of music already playing
        if(isMusicPlaying) {
            Log.d(TAG,"Music already playing, returning")
            return
        }
        //load random or resume
        if (isSongLoaded) {
            //resume song
            Log.d(TAG, "Music already loaded, resuming")
            player.start()
            isMusicPlaying = true
            updateTimeNotificationHandler.sendEmptyMessage(MSG_UPDATE_TIME)

        } else {
            //load new song
            Log.d(TAG,"Song is not loaded, loading and starting")
            //check for null
            val path: String = getRandomSong() ?: return

            isSongFinished = false
            player.setDataSource(assets.openFd(path))
            player.prepare()
            player.start()
            currentTrackDurationIntSec  = player.duration / 1000
            currentTrackDurationString = timeFormatter(player.duration)
            isMusicPlaying = true
            isSongLoaded = true
            updateTimeNotificationHandler.sendEmptyMessage(MSG_UPDATE_TIME)
            updateNotification()


        }

    }

    //stop button function
    fun stopTrack() {
        Log.d(TAG,"Stopping track")
        if (isSongLoaded ) {
            player.stop()
            player.reset()
            isSongLoaded = false
            isMusicPlaying = false
            updateTimeNotificationHandler.removeMessages(MSG_UPDATE_TIME)
            currentTitle = ""
            currentTrackDurationString = ""
            currentTrackDurationIntSec = 0
            updateNotification()


        }
    }

    //pause button function
    fun pauseTrack() {
        Log.d(TAG,"Pausing track")
        if(isMusicPlaying) {
            updateTimeNotificationHandler.removeMessages(MSG_UPDATE_TIME)
            player.pause()
            isMusicPlaying = false
            updateNotification()

        }
    }

    //exit button function
    fun exitApp() {
        Log.d(TAG,"Exiting track")
        stopForeground(true)
        gesturesOff()
        stopSelf()
        exitProcess(0)
    }

    //returns formatted time
    fun currentTimeString(): String{
        return timeFormatter(player.currentPosition)
    }

    //converts time to seconds
    fun currentTimeIntSec(): Int{
        return player.currentPosition/1000
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "Destroying service")
    }

    //update notification in foreground
    fun updateNotification() {
        startForeground(NOTIFICATION_ID, createNotificationFunction())

    }


    //handler to update notification when music is playing
    private val updateTimeNotificationHandler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            if (MSG_UPDATE_TIME == msg.what) {
                updateNotification()
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS)
            }
        }
    }

    //returns the path to a song
    private fun getRandomSong(): String? {
        val songList = assets.list("music")
        //check for songs in assets/music, if none, return null
        val len = songList?.size ?: -1
        if (len < 0) {
            return null
        }
        Log.d(TAG, "Random song chosen, length of list $len")
        //set global title variable
        currentTitle = (songList?.get((0 until len).random())).toString()
        return "music/$currentTitle"
    }

    // create channel
    private fun createNotificationChannel() {
        val channel = NotificationChannel(channelID, getString(R.string.channel_name),
        NotificationManager.IMPORTANCE_LOW)
        channel.description = getString(R.string.channel_description)
        val managerCompat = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        managerCompat.createNotificationChannel(channel)
    }

    //used to call service functions from notification in foreground
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG,"On start")
        if(intent.action == ACTION_STOP) {
            lastCommand = true
            stopTrack()
        }

        if(intent.action == ACTION_TOGGLE) {
            if(isMusicPlaying) {
                lastCommand = true
                pauseTrack()

            } else {
                lastCommand = true
                startTrack()
            }

        }

        if(intent.action == ACTION_EXIT) {
            exitApp()
        }
        return START_STICKY
    }

    //notification setup, adds 3 buttons, text varies based on certain conditions
    private fun createNotificationFunction(): Notification{
        val stopIntent =  Intent(this, MediaPlayerService::class.java)
        val toggleIntent =  Intent(this, MediaPlayerService::class.java)
        val exitIntent =  Intent(this, MediaPlayerService::class.java)
        stopIntent.action = ACTION_STOP
        toggleIntent.action = ACTION_TOGGLE
        exitIntent.action = ACTION_EXIT

        val stopPendingIntent = PendingIntent.getService(this,0, stopIntent,
        PendingIntent.FLAG_IMMUTABLE)
        val togglePendingIntent = PendingIntent.getService(this,1, toggleIntent,
        PendingIntent.FLAG_IMMUTABLE)
        val exitPendingIntent = PendingIntent.getService(this,2, exitIntent,
        PendingIntent.FLAG_IMMUTABLE)

        val toggleText = if (isMusicPlaying) {
            "Pause"
        } else { "Play" }

        val displayTimeNotification = if (isSongFinished) {
            "$currentTrackDurationString/"
        } else if (!isSongLoaded) {""} else {
            currentTimeString() + "/"
        }
        val playingOrNot = if (isSongLoaded) { "Now playing "} else { "Nothing is playing"}
        val builder = NotificationCompat.Builder(this, channelID)
            .setContentTitle("$playingOrNot$currentTitle")
            .setContentText("$displayTimeNotification$currentTrackDurationString")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setChannelId(channelID)
            .addAction(android.R.drawable.btn_default, toggleText, togglePendingIntent)
            .addAction(android.R.drawable.btn_default, "Stop", stopPendingIntent)
            .addAction(android.R.drawable.btn_default, "Exit", exitPendingIntent)

        return builder.build()
    }

    //formats milliseconds to be readable by humans
    private fun timeFormatter(milliseconds: Int): String {
        val hours = ((milliseconds / 1000) / 60) / 60
        val minutes = ((((milliseconds / 1000) / 60))) - (hours * 60)
        val seconds = ((((milliseconds / 1000)))) - (minutes * 60) - (hours * 60 * 60)
        val h: String = if (hours < 10) {
            "0$hours"
        } else {
            hours.toString()
        }
        val m: String = if (minutes < 10) {
            "0$minutes"
        } else {
            minutes.toString()
        }
        val s: String = if (seconds < 10) {
            "0$seconds"
        } else {
            seconds.toString()
        }
        return "$h:$m:$s"
    }

}
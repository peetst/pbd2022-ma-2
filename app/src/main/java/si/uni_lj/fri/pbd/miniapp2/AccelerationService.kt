package si.uni_lj.fri.pbd.miniapp2

import android.app.Service
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Binder
import android.os.IBinder
import android.util.Log

class AccelerationService : Service(),  SensorEventListener {
    companion object {
        val TAG = AccelerationService::class.simpleName
        const val SENSOR_NOISE_TRSH = 3
    }

    var changePlay = false
    var changePause = false
    private val serviceBinder = RunServiceBinder()

    private var sensorManager: SensorManager? = null
    private var acceleSensor: Sensor? = null

    private var prevX: Float? = null
    private var prevY: Float? = null
    private var prevZ: Float? = null

    inner class RunServiceBinder: Binder() {
        val service:  AccelerationService
            get() = this@AccelerationService
    }

    override fun onBind(intent: Intent): IBinder {
        return serviceBinder
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG,"Stopping self")
        sensorManager?.unregisterListener(this)
        stopSelf()

    }
    //assign sensors etc.
    override fun onCreate() {
        Log.d(TAG, "Creating")
         sensorManager =
            getSystemService(Context.SENSOR_SERVICE) as SensorManager
        Log.d(TAG, "SensorManager created")
        acceleSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        Log.d(TAG, "acceleSensor created")
        sensorManager?.registerListener(this,acceleSensor, SensorManager.SENSOR_DELAY_NORMAL)
        Log.d(TAG, "Listener created")
        super.onCreate()
    }

    //check sensor changes, change variables accordingly to notify MediaPlayerService
    override fun onSensorChanged(p0: SensorEvent?) {
        val sensorValues = p0?.values?.copyOf()
        val xValue = sensorValues?.get(0)
        val yValue = sensorValues?.get(1)
        val zValue = sensorValues?.get(2)

        if (prevX == null || prevY == null || prevZ == null) {
            Log.d(TAG, "VARIABLES STILL NULL")

            prevX = xValue
            prevY = yValue
            prevZ = zValue
        } else {
            var diffX = kotlin.math.abs(xValue!! - prevX!!)
            var diffZ = kotlin.math.abs(zValue!! - prevZ!!)
            if (diffX <= SENSOR_NOISE_TRSH) {
                diffX = 0F
            }
            if (diffZ <= SENSOR_NOISE_TRSH) {
                diffZ = 0F
            }
            if (diffX > diffZ) {
                Log.d(TAG, "DETECTED X>Z")
                changePause = true
            } else if (diffX < diffZ) {
                Log.d(TAG, "DETECTED X<Z")
                changePlay = true
            }

        }
        prevX = xValue
        prevY = yValue
        prevZ = zValue


    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
        Log.d(TAG, "Accuracy changed")
    }


}
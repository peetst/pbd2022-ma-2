package si.uni_lj.fri.pbd.miniapp2

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import si.uni_lj.fri.pbd.miniapp2.MediaPlayerService.Companion.ACTION_START
import si.uni_lj.fri.pbd.miniapp2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    companion object {
        val TAG = MainActivity::class.simpleName
        //for timers
        const val ACTION_EXIT = "stop_play_service"
        private const val MSG_UPDATE_TIME = 1
        private const val UPDATE_RATE_MS = 1000L
    }

    //scope for coroutine
    private val progressBarScope: CoroutineScope = CoroutineScope(Dispatchers.Default)

    private lateinit var  mainBind: ActivityMainBinding
    var serviceBound = false
    var mediaPlayerService: MediaPlayerService? = null

    //service connection
    private val servConnect: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binder = p1 as MediaPlayerService.RunServiceBinder
            mediaPlayerService = binder.service
            serviceBound = true
            Log.d(TAG, "Service bound")
            //first click doesn't start track, callback a function to fix:
            //playFunctionCaller()
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            Log.d(TAG, "Service Unbound")
            serviceBound = false
        }
    }



    //handler to update UI when music is playing
    private val updateTimeHandler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            if (MSG_UPDATE_TIME == msg.what) {
                updateUITimer()
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS)
            }
        }
    }


    //function to update timer in ui
    private fun updateUITimer() {
        if(serviceBound) {

             val totalTime = mediaPlayerService?.currentTrackDurationString
             //display time equal to max if song over
             val currentTime = if (mediaPlayerService?.isSongFinished == false) {
                 mediaPlayerService?.currentTimeString()
             } else {
                 totalTime
             }
            //display time if a song is currently loaded
            mainBind.trackTimerText.text =  if (mediaPlayerService?.isSongLoaded == true) {
                getString(R.string.timerDisplay, currentTime, totalTime)
            } else {""}

            //exception to update UI in specific case
            if (mediaPlayerService?.lastCommand == true) {
                updateUI()
                mediaPlayerService?.lastCommand = false
            }

        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBind = ActivityMainBinding.inflate(layoutInflater)
        val view = mainBind.root
        setContentView(view)

        serviceConnector()

        ///set listeners for buttons that call their respective functions in service
        mainBind.exitBtn.setOnClickListener {
            if (mediaPlayerService?.isMusicPlaying == true) {
                mediaPlayerService?.stopTrack()
            }
                mediaPlayerService?.exitApp()
                finishAndRemoveTask()

        }
        mainBind.playBtn.setOnClickListener {
            mediaPlayerService?.lastCommand = false
            playFunctionCaller()


        }
        mainBind.stopBtn.setOnClickListener {
            mediaPlayerService?.lastCommand = false
            mediaPlayerService?.stopTrack()
            updateTimeHandler.removeMessages(MSG_UPDATE_TIME)
            updateUI()
            clearTimer()

        }
        mainBind.pauseBtn.setOnClickListener {
            mediaPlayerService?.lastCommand = false
            mediaPlayerService?.pauseTrack()
            updateTimeHandler.removeMessages(MSG_UPDATE_TIME)
            updateUI()
        }

        mainBind.gesturesOffBtn.setOnClickListener {
            mediaPlayerService?.gesturesOff()
        }
        mainBind.gesturesOnBtn.setOnClickListener {
            mediaPlayerService?.gesturesOn()
        }

    }

    override fun onResume() {
        Log.d(TAG, "Resuming activity")
        //on resume update the UI
        super.onResume()
        if (mediaPlayerService?.isSongLoaded == true) {
            updateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME)
            mainBind.barProgress.visibility = View.VISIBLE
            mainBind.trackTitleText.text = mediaPlayerService?.currentTitle ?: ""
        }

    }

    //clear timer helper function
    private fun clearTimer() {
        mainBind.trackTimerText.text = ""
    }

    //update UI for stop/pause/play
    private fun updateUI() {
        mainBind.trackTitleText.text = mediaPlayerService?.currentTitle ?: ""
        mainBind.barProgress.visibility = if (mediaPlayerService?.isSongLoaded == false ) {
            View.INVISIBLE
        }  else {
            View.VISIBLE
        }
    }

    //unbind service etc.
    override fun onDestroy() {
        super.onDestroy()
        val i = Intent(this, MediaPlayerService::class.java)
        i.action = ACTION_EXIT
        stopService(i)
        unbindService(servConnect)
    }

    //function to start service, in case of it not being started
    private fun serviceConnector() {
        Log.d(TAG, "Connecting service")
        val i = Intent(this, MediaPlayerService::class.java)
        i.action = ACTION_START
        startService(i)
        bindService(i, servConnect, 0)
    }

    ///helper function called when a new song is played/resumed
    private fun playFunctionCaller() {
        //set progressbar size to track duration if song
        Log.d(TAG, "Play pressed, calling functions")
        //start track
        mediaPlayerService?.startTrack()
        updateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME)
        updateUI()
        if(mediaPlayerService?.isSongFinished == true) {
            mainBind.barProgress.progress = 0
        }

        progressBarScope.launch { progressBarUpdater() }

    }


    //coroutine to update progress bar
    private suspend fun progressBarUpdater() {
        withContext(Dispatchers.Main) {
            mainBind.barProgress.visibility = View.VISIBLE


        }
        for (i in 0..mainBind.barProgress.max) {
            try {
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
                continue
            }
            //every second update the UI in main thread
            withContext(Dispatchers.Main) {
                if(mediaPlayerService?.lastCommand == true) {
                    mediaPlayerService?.lastCommand = false
                    updateUI()
                    updateUITimer()
                }
                mainBind.barProgress.max = mediaPlayerService?.currentTrackDurationIntSec!!
                mainBind.barProgress.progress = mediaPlayerService?.currentTimeIntSec()!!
                if(mediaPlayerService?.isSongFinished == true) {
                    mainBind.barProgress.progress = mainBind.barProgress.max
                }
            }
        }


    }


}